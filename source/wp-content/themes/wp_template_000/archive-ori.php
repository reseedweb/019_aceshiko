<?php get_header(); ?>
	<section id="breadcrumb">
		<?php if(function_exists('bcn_display'))
		{
			bcn_display();
		}?>               
	</section> 
	
	<section id="main-h1-title">
        <h1 class="h1-title">折り方から選ぶ</h1>
	</section>
	<?php $posts = get_posts(array(
		'post_type'=> 'ori',	
		'posts_per_page' => 50,
		'paged' => get_query_var('paged'),
	   ));		
	  ?>	
	<div class="ori-category-navi">	       		
        <ul class="clearfix">
			<?php $i = 0; ?>
			<?php foreach($posts as $p) :  ?>
				<li><a href="#<?php echo $p->post_title; ?>">  <?php echo $p->post_title; ?></a></li>					
			<?php endforeach; ?>
		</ul>		
	</div>
	
	<?php		
	$ori_term_count = 0;
	foreach($posts as $post) :
		$ori_term_count = $ori_term_count + 1; 
	?>
	<div id="<?php echo $post->post_title; ?>" class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title"><?php echo $post->post_title; ?></h2>  
		<div class="ori-content clearfix">
			<div class="message-right message-230 clearfix">
				<div class="image">
					<?php echo get_the_post_thumbnail($post->ID,'large'); ?>
				</div>
				<div class="text ln2em">
					<?php echo apply_filters('the_content', $post->post_content ); ?>	
				</div>						
			</div>
			
			<div class="message-group ori-gallery"><!-- begin message-group -->		
				<div class="message-row clearfix">						      
				<?php echo get_field('ori-gallery', $post->ID); ?>
					<div class="message-col message-col230">			
						<div class="image">							
						</div>						  					        	            					
					</div>					
				</div>					
			</div>
			
			<div class="ori-cate clearfix">
				<div class="ori-cate-title">おすすめ用途例</div>
				<div class="ori-cate-info">
					<ul class="clearfix">
					<?php 
						$all_tags_posts = wp_get_post_tags($post->ID);						
						foreach ($all_tags_posts as $tags_post):
						?>
						<li>
							<?php echo $tags_post->name;?>

						</li>					
					<?php endforeach; ?>
					</ul>					
				</div>
			</div>
			
			<div class="ori-cate clearfix">
				<div class="ori-cate-title">こんなご要望に</div>
				<div class="ori-cate-info">
					<ul class="clearfix">										
						<li>
							<a href="<?php bloginfo('url'); ?>/case"><?php @the_terms($post->ID, 'cat-ori'); ?></a>
						</li>						
					</ul>					
				</div>
				
			</div>		
		</div>		
    </div><!-- end primary-row -->				   
	<?php endforeach; ?>
	
	<?php wp_reset_query(); ?>
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>