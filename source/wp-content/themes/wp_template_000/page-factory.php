<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>	           
<div class="primary-row clearfix"><!-- begin primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">折り加工の様子</h2>
		<div class="factory-content-first">
			<p class="factory-info">自社工場での折り加工の様子を写真と動画を交えてご紹介いたします。</p>
			<h3 class="factory-title">工場</h3>	
			<p class="factory-info">工場全体の様子です。１階は主に断裁と折り加工、２階ではミニ折りなどの加工を行っております。</p>		
			<div class="message-group clearfix"><!-- begin message-group -->
				<div class="message-row clearfix"><!--message-row -->												
					<div class="message-col message-col355 clearfix ">					
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img1.jpg" alt="message col" />
						</div><!-- end image -->						
					</div><!-- end message-col -->	
					<div class="message-col message-col355 clearfix">					
						<div class="image">
							<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img2.jpg" alt="message col" />
						</div><!-- end image -->						
					</div><!-- end message-col -->				
				</div><!-- end message-row -->				
			</div><!-- end message-group -->
		</div>		
	</div>
	
	<div class="factory-content clearfix"><!-- begin primary-row -->				
		<h3 class="factory-title">断裁</h3>	
		<p class="factory-info">紙の断裁をしているところです。</p>		
		<div class="message-group clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->												
				<div class="message-col message-col355 ">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img3.jpg" alt="message col" />
					</div><!-- end image -->						
				</div><!-- end message-col -->	
				<div class="message-col message-col355">					
					<div class="image">
						<iframe width="355" height="220" src="//www.youtube.com/embed/5U7a_Pmi-iw" frameborder="0" allowfullscreen></iframe>
					</div><!-- end image -->						
				</div><!-- end message-col -->				
			</div><!-- end message-row -->				
		</div><!-- end message-group -->
	</div>
	
	<div class="factory-content clearfix"><!-- begin primary-row -->				
		<h3 class="factory-title">折り加工</h3>	
		<p class="factory-info">用途に合わせて様々な折り加工が施されていきます。折り加工専門のスタッフが正確丁寧に作業いたします。</p>		
		<div class="message-group clearfix"><!-- begin message-group -->
			<div class="message-row clearfix"><!--message-row -->												
				<div class="message-col message-col355 ">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img4.jpg" alt="message col" />
					</div><!-- end image -->						
				</div><!-- end message-col -->	
				<div class="message-col message-col355">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img5.jpg" alt="message col" />
					</div><!-- end image -->						
				</div><!-- end message-col -->				
			</div><!-- end message-row -->				
			<div class="message-row clearfix"><!--message-row -->												
				<div class="message-col message-col355 ">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img6.jpg" alt="message col" />
					</div><!-- end image -->						
				</div><!-- end message-col -->	
				<div class="message-col message-col355">					
					<div class="image">
						<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img7.jpg" alt="message col" />
					</div><!-- end image -->						
				</div><!-- end message-col -->				
			</div><!-- end message-row -->				
		</div><!-- end message-group -->
		<p class="pt20">
			<iframe width="720" height="446" src="//www.youtube.com/embed/5U7a_Pmi-iw" frameborder="0" allowfullscreen></iframe>
		</p>		
	</div>
</div>
	
	
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">スタッフからのメッセージ</h2>  
		<div class="ori-content clearfix">
			<div class="message-right message-300 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/factory_content_img8.jpg" alt="message col" />
				</div>
				<div class="text ln2em">
					私たちが責任持って、折り加工・検品いたします！<br />
					弊社は、折り加工・断裁専門の加工会社です。<br />
					自社工場で加工に携わるスタッフは約20名ほど。１日に数万枚の印刷物が折りや断裁などの加工を施されていきます。<br />
					最新鋭の機材と熟練スタッフによって正確に・素早く対応します。折り加工のプロにすべてお任せください！
				</div>						
			</div>				
		</div>		
    </div><!-- end primary-row -->		
	
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>

