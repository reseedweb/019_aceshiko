<div id="sideMenu">
	<div class="sideMenu_title">ご要望から選ぶ</div>
	<?php	
		$ori_terms = get_terms('cat-ori',array(
			'hide_empty'    => false,
			'parent' => 0,
			'orderby'           => 'slug', 
			'order'             => 'ASC',    
		));
	?>
		<ul>
			<?php			
			$ori_navi_term_count = 0;
			foreach($ori_terms as $ori_term) :
				$ori_navi_term_count = $ori_navi_term_count + 1; 
			?>
				<li><a href="case#<?php echo $ori_term->name; ?>"><?php echo $ori_term->name; ?></a></li>
			<?php endforeach; ?>		
		</ul>
	<div class="mb10"></div>
	<div class="sideMenu_title">折り方から選ぶ</div>
	<?php $posts = get_posts(array(
		'post_type'=> 'ori',	
		'posts_per_page'=> 30,
		'paged' => get_query_var('paged'),
	   ));		
	  ?>	
	<ul>
		<?php $i = 0; ?>
		<?php foreach($posts as $p) :  ?>
			<li><a href="ori#<?php echo $p->post_title; ?>">  <?php echo $p->post_title; ?></a></li>					
		<?php endforeach; ?>
	</ul>		
	
</div>