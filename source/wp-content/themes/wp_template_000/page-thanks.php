<?php get_template_part('header'); ?>	
    <div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title">送信されました</span></h2>
		<p class="mb20 ln2em">
			お問い合わせ・お見積もりありがとうございました。専門スタッフがメールを確認後、即日対応させて頂きます。<br />	
			（営業時間終了後の場合はご返信は翌営業日になります。ご了承ください）<br />
			１週間たっても返信がない場合、お急ぎの場合は、お手数ですが下記電話番号までご連絡くださいませ。<br />
			 TEL：06-6746-0839 / FAX：06-6746-0864
		</p>
    </div><!-- end primary-row -->
	<div class="primary-row clearfix"><!-- begin primary-row -->
	<?php get_template_part('part','contact'); ?>
	</div><!-- end primary-row -->	
<?php get_template_part('footer'); ?>