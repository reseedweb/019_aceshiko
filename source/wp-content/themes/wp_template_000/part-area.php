<div class="region">
	<ul>
	<?php
	if(class_exists('CustomRegion')){
		$custom_region = CustomRegion::get_instance();
		echo $custom_region->get_region_panel();	
	}
	?>
	</ul>
</div>    	
<script type="text/javascript">
$(document).ready(function(){
	$('.region > ul > li > ul > li > span').prepend('<strong>[</strong>');
	$('.region > ul > li > ul > li > span').append('<strong>]</strong>');
});
</script>