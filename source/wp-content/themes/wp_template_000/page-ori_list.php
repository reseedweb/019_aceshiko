<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb'); ?>	           
	<div class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title"><?php the_title(); ?></h2>  					    	
		<?php $posts = get_posts(array(
				'post_type'=> 'ori',
				'posts_per_page' => 50,
				'paged' => get_query_var('paged'),
			));		
		?>	
	    <div class="case-content">							
			<div class="case-group"><!-- begin case-group -->					
			<?php $i = 0; ?>
			<?php foreach($posts as $p) :  ?>	  
				<?php $i++; ?>
				<?php if($i%3 == 1) : ?>
				<div class="case-row clearfix">
				<?php endif; ?>			      
					<div class="case-col case-col230">			
						<div class="image">
							<a href="ori#<?php echo $p->post_title; ?>"><?php echo get_the_post_thumbnail($p->ID,'large'); ?></a>
						</div><!-- end image -->
						<div class="title">
						   <?php echo $p->post_title; ?>
						</div><!-- end title -->  					        	            					
					</div><!-- end case-col -->
					<?php if($i%3 == 0 || $i == count($posts) ) : ?>
				</div><!-- end case-row -->
				<?php endif; ?>
			<?php endforeach; ?>		
			</div>
		</div>		
		<?php wp_reset_query(); ?>	    		
	</div><!-- end primary-row -->

<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>