<div class="sidebar-row"><!-- begin sidebar-row -->    
    <a href="#"><img alt="side bnr" src="<?php bloginfo('template_url');?>/img/common/side_bnr1.jpg" /></a>
</div><!-- end sidebar-row -->
<div class="sidebar-row"><!-- begin sidebar-row -->
    <?php get_template_part('part','sideMenu'); ?>
</div><!-- end sidebar-row -->
<div class="sidebar-row"><!-- begin sidebar-row -->
    <div class="sideCompany">
        <div class="title">
            <span class="small">運営会社</span>
            <span class="large">有限会社エース紙工</span>
        </div>
        <p class="text-center">
        <img alt="side tel" 
            src="<?php bloginfo('template_url');?>/img/common/side_tel.jpg" />
        </p>
        <p class="text-center">
        <img alt="side con" 
            src="<?php bloginfo('template_url');?>/img/common/side_con.jpg" />
        </p>       
        <p class="mt10 mb10">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Cum alias, eaque perspiciatis eum tempora libero aut eos ab repellendus quisquam quam ut, reiciendis pariatur incidunt fugiat, doloremque tenetur explicabo numquam.
        </p>
        <div>
            <a class="link" href="<?php bloginfo('url'); ?>/company"><span>会社案内はコチラから</span></a>
        </div> 
    </div>
</div>
<div class="sidebar-row"><!-- begin sidebar-row -->
    <a href="<?php bloginfo('url'); ?>/factory">
        <img alt="factory" 
        src="<?php bloginfo('template_url');?>/img/common/side_bnr2.jpg" />                         
    </a>
</div><!-- end sidebar-row -->
<div class="sidebar-row"><!-- begin sidebar-row -->
    <a href="<?php bloginfo('url'); ?>/service#service1">
        <img alt="service" 
        src="<?php bloginfo('template_url');?>/img/common/side_bnr3.jpg" />                         
    </a>
</div><!-- end sidebar-row -->
<div class="sidebar-row"><!-- begin sidebar-row -->
    <a href="<?php bloginfo('url'); ?>/service#service2">
        <img alt="service" 
        src="<?php bloginfo('template_url');?>/img/common/side_bnr4.jpg" />                         
    </a>
</div><!-- end sidebar-row -->
<div class="sidebar-row"><!-- begin sidebar-row -->
    <a href="<?php bloginfo('url'); ?>/service#service3">
        <img alt="service" 
        src="<?php bloginfo('template_url');?>/img/common/side_bnr5.jpg" />                         
    </a>
</div><!-- end sidebar-row -->