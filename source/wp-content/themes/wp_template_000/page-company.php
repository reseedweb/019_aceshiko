<?php get_header(); ?>
	<div class="primary-row clearfix">
		<h2 class="h2_title">会社概要</h2>
		<table class="company_table">
			<tr>
				<th>会社名</th>
				<td>有限会社　エース紙工</td>
			</tr>
			<tr>
				<th>代表者</th>
				<td>代表取締役　福本　喜作</td>
			</tr>
			<tr>
				<th>住所</th>
				<td>5/6/1992</td>
			</tr>
			<tr>
				<th>資本金</th>
				<td>3,000,000円</td>
			</tr>
			<tr>
				<th>住所</th>
				<td>本社 〒578-0951　大阪府東大阪市新庄東6番17号</td>
			</tr>
			<tr>
				<th>連絡先</th>
				<td> TEL：06-6746-0839　FAX：06-6746-0864</td>
			</tr>						
			<tr>
				<th>業種	</th>
				<td>
					印刷・各種裁断・折り加工
				</td>
			</tr>
			<tr>
				<th>主要取引先</th>
				<td>
					国際印刷工業株式会社<br/>
					株式会社　コーヨー21	<br/>
					三和実業株式会社	<br/>
					鈴木美術印刷株式会社	<br/>
					日邦印刷（アイ・エヌ・ティ株式会社）	<br/>
					日本アート印刷工業株式会社	<br/>
					株式会社　秀永	<br/>
					日本マーク印刷株式会社<br/>	
					有限会社　宏栄紙工	<br/>
					株式会社　朝栄社	<br/>
					株式会社　明和印刷	<br/>
					株式会社　日東印刷	<br/>
					サン工芸印刷株式会社						
				</td>
			</tr>			
		</table>
	</div>
	
	<div class="primary-row clearfix">
		<h2 class="h2_title">沿革</h2>
		<table class="company_table">
			<tr>
				<th>平成4年5月</th>
				<td>東大阪市古箕輪に設立創業。</td>
			</tr>
			<tr>
				<th>平成8年8月</th>
				<td>資本金300万に増資し、有限会社エース紙工を設立。</td>
			</tr>
			<tr>
				<th>平成14年9月</th>
				<td>事業拡大に伴い、現在の所在地に工場を移転。</td>
			</tr>
		</table>
	</div>
	
	<div class="primary-row clearfix">
		<h2 class="h2_title">会社概要</h2>
		<table class="company_table_last">
			<tr>
				<th>販売価格</th>
				<td>各商品毎に提示</td>
			</tr>
			<tr>
				<th>ご注文方法</th>
				<td>電話、FAX、お問い合わせフォーム</td>
			</tr>
			<tr>
				<th>お支払い方法</th>
				<td>受注時前払い(銀行振込)</td>
			</tr>
			<tr>
				<th> 
					商品代金以外<br />
					の必要料金
				</th>
				<td>
					消費税、振込手数料					
				</td>
			</tr>
			<tr>
				<th>商品の配送</th>
				<td>日本国内、海外配送に関して一部地域への配送が可能です。 詳しくは、お問合わせください。</td>
			</tr>
			<tr>
				<th>返品・交換</th>
				<td>不良品以外の返品・交換はお受けできません。不良品等、弊社責任範囲によるものは無条件でお受けいたします。</td>
			</tr>						
			<tr>
				<th>キャンセル</th>
				<td>デザイン校了前であればキャンセルできますが、校了後のキャンセルはできません。進行している状況に応じ、かかった費用分をご請求させていただきます。</td>
			</tr>				
		</table>
	</div>
	
	<div class="primary-row clearfix">
		<h2 class="h2_title">アクセス</h2>
		<div id="company_map">
			<iframe src="https://www.google.com/maps/embed?pb=!1m20!1m8!1m3!1d3280.6788502961736!2d135.60488269710538!3d34.688053978271185!3m2!1i1024!2i768!4f13.1!4m9!1i0!3e6!4m0!4m5!1s0x6001200f6355da97%3A0x88047ea285460417!2s6-17+Shinj%C5%8Dhigashi%2C+Higashi%C5%8Dsaka-shi%2C+%C5%8Csaka-fu%2C+Japan!3m2!1d34.688054!2d135.60718939999998!5e0!3m2!1sen!2sus!4v1412935639722" width="760" height="300" frameborder="0" style="border:0"></iframe>
		</div>
	</div>
<?php get_footer(); ?>