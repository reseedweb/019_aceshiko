<?php get_header(); ?>
	<?php get_template_part('part','breadcrumb'); ?>	           
	<?php	
		$ori_terms = get_terms('cat-ori',array(
			'hide_empty'    => false,
			'parent' => 0,
			'orderby'           => 'slug', 
			'order'             => 'ASC',    
		));
	?>
	<div class="case-category-navi">	
        <ul class="clearfix">
		<?php			
		$ori_navi_term_count = 0;
		foreach($ori_terms as $ori_term) :
			$ori_navi_term_count = $ori_navi_term_count + 1; 
		?>
			<li><a href="#<?php echo $ori_term->name; ?>"><?php echo $ori_term->name; ?></a></li>			
		<?php endforeach; ?>
		</ul>
	</div>
	
	<?php		
	$ori_term_count = 0;
	foreach($ori_terms as $ori_term) :
		$ori_term_count = $ori_term_count + 1; 
	?>
    <div id="<?php echo $ori_term->name; ?>" class="primary-row clearfix"><!-- begin primary-row -->
		<h2 class="h2_title"><?php echo $ori_term->name; ?></h2>  
		<div class="case-content">
			<div class="ln2em pb10">
				<?php echo $ori_term->description; ?>
			</div>
			<h3 class="case-title">おすすめの折り方</h3>					
			<?php	
			$ori_posts = get_posts( array(
				'post_type'=> 'ori',
				'posts_per_page' => 4,
				'paged' => get_query_var('paged'),
				'tax_query' => array(
					array(
					'taxonomy' => 'cat-ori', 
					'field' => 'term_id', 
					'terms' => $ori_term->term_id))	    
			));
			?>
			<div class="case-group"><!-- begin case-group -->			
			<?php $i = 0;?>
			<?php foreach($ori_posts as $ori_post):	?>
			<?php $i++; ?>
				<?php if($i%3 == 1) : ?>
				<div class="case-row clearfix">		
				<?php endif; ?>	
					<div class="case-col case-col230">
						<div class="image">
							<?php echo get_the_post_thumbnail($ori_post->ID,'large'); ?>
						</div><!-- end image -->
						<div class="title">
						   <?php echo $ori_post->post_title; ?>
						</div><!-- end title -->                
					</div><!-- end case-col -->					
				 <?php if($i%3 == 0 || $i == count($ori_posts) ) : ?>
				</div>				        
			<?php endif; ?>
			<?php endforeach; ?> 
			</div><!-- end case-group -->			
		</div>	
	<?php wp_reset_query(); ?>		
    </div><!-- end primary-row -->		
	<?php endforeach; ?>
	<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>