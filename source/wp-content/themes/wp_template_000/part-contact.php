<div class="primary-row clearfix">
	<h2 class="h2_title">お問い合わせはこちらから</h2>
	<div class="part-contact">
		<img src="<?php bloginfo('template_url'); ?>/img/common/part_con_bg.jpg" alt="contact" />
		<div class="part-contact-btn">
			<a href="<?php bloginfo('url'); ?>/contact">
				<img src="<?php bloginfo('template_url'); ?>/img/common/part_con_btn.jpg" alt="contact" />
			</a>
		</div>
	</div>
</div>