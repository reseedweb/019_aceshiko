<section id="mainslide">
    <div class="container"><!-- begin container -->
        <div class="row clearfix"><!-- begin row -->
            <div class="col-md-18"><!-- begin col -->
            	<div class="mainslide-content">
					<ul class="bxslider">
					  <li><img src="<?php bloginfo('template_url'); ?>/img/top/mainslide_01.jpg" /></li>
					  <li><img src="<?php bloginfo('template_url'); ?>/img/top/mainslide_02.jpg" /></li>
					  <li><img src="<?php bloginfo('template_url'); ?>/img/top/mainslide_03.jpg" /></li>
					</ul>

					<div id="bx-pager">
					  <a data-slide-index="0" href="#"><img src="<?php bloginfo('template_url'); ?>/img/top/mainslide_01.jpg" /></a>
					  <a data-slide-index="1" href="#"><img src="<?php bloginfo('template_url'); ?>/img/top/mainslide_02.jpg" /></a>
					  <a data-slide-index="2" href="#"><img src="<?php bloginfo('template_url'); ?>/img/top/mainslide_03.jpg" /></a>
					</div>
				</div>                
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</section>