<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>	   
<div class="primary-row clearfix"><!-- begin primary-row -->
	<h2 class="h2_title">お問い合わせから納品まの流れ</h2>  
	<div class="flow-bg">
		<div class="primary-row clearfix">			
		<div class="message-right message-250 clearfix">
			<div class="image">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img1.jpg" alt="page feature" />
			</div>
			<div class="text ln2em">
				<div class="flow-title clearfix">
					<div class="flow-step">Step<span class="flow-step-no">01</span></div><h3 class="flow-step-title">お問い合わせ・お見積り</h3>
				</div>
				<p>
					まずはお気軽にお問い合わせください。<br />
					その際、オリジナル冊子の仕様をある程度決めて頂けると、<br/>
					打ち合わせがスムーズです。<br />
					もし冊子印刷や仕様のことで迷っていること・疑問点がございましたら、お気軽にお尋ねください。冊子印刷のコンシェルジュがお客様のご要望をお聞きし、一緒に仕様を決めていきます。
				</p>
				<div class="pt10">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_tel.jpg" alt="message col" />
					<a href="<?php bloginfo('url'); ?>/contact">
						<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_con.jpg" alt="message col" />
					</a>					
				</div>	
			</div>			
		</div>
		<div class="flow-arrow">
			<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="page feature" />
		</div>
		</div>	
		
		<div class="primary-row clearfix">			
			<div class="message-right message-250 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img2.jpg" alt="page feature" />
				</div>
				<div class="text ln2em">
					<div class="flow-title clearfix">
						<div class="flow-step">Step<span class="flow-step-no">02</span></div><h3 class="flow-step-title">ご注文</h3>
					</div>
					<p>
						打ち合わせ内容でお聞きしたご要望を元に、弊社から加工のお見積り価格の提示をいたします。<br />
						納得いただければ、正式にご注文となります。<br />
						基本的に全額銀行振込・前払い制を取らせていただいております。ご了承ください。お支払い方法のご相談もできますので、担当のスタッフにお申し付けください。
					</p>				
				</div>			
			</div>
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="page feature" />
			</div>
		</div>	
		
		<div class="primary-row clearfix">			
			<div class="message-right message-250 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img3.jpg" alt="page feature" />
				</div>
				<div class="text ln2em">
					<div class="flow-title clearfix">
						<div class="flow-step">Step<span class="flow-step-no">03</span></div><h3 class="flow-step-title">印刷物支給</h3>
					</div>
					<p>
						印刷物の支給をお願いします。<br />
						配送先は下記の住所にて承ります。
					<p>
					<p class="pt20">
						〒578-0951　大阪府東大阪市新庄東6番17号
					</p>				
				</div>			
			</div>
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="page flow" />
			</div>
		</div>	
		
		<div class="primary-row clearfix">			
			<div class="message-right message-250 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img4.jpg" alt="page flow" />
				</div>
				<div class="text ln2em">
					<div class="flow-title clearfix">
						<div class="flow-step">Step<span class="flow-step-no">04</span></div><h3 class="flow-step-title">折り加工</h3>
					</div>
					<p>
						ご注文の指定通りに「折り加工」を施します。<br />
						折り目が美しいか、折った後の角が揃っているか、丁寧に検品を行いながら加工していきます。<br />
						工場内の様子はこちらのページで詳しくご覧になることができます。<br />
						折り加工の工場見学も歓迎しています
					</p>				
				</div>			
			</div>
			<div class="flow-arrow">
				<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_arrow.jpg" alt="page flow" />
			</div>
		</div>

		<div class="primary-row clearfix">			
			<div class="message-right message-250 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/flow_content_img5.jpg" alt="page flow" />
				</div>
				<div class="text ln2em">
					<div class="flow-title clearfix">
						<div class="flow-step">Step<span class="flow-step-no">05</span></div><h3 class="flow-step-title">納品</h3>
					</div>
					<p>
						仕上がった製品を指定のご住所まで発送するか、工場から引き取りいただき、納品いたします。<br />
						納品後、折り加工について気になる点・不具合などありましたら、迅速に対応させていただきます。
					</p>				
				</div>			
			</div>		
		</div>	
	</div>	
</div>

<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>