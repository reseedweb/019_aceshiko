<?php get_template_part('header'); ?>
<div class="primary-row">
	<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
	<h2 class="h2_title"><?php the_title(); ?></h2>
 	<div class="post-row-meta">
        <i class="fa fa-clock-o"><?php the_time('l, F jS, Y'); ?></i>
        <i class="fa fa-tags"></i><?php the_category(' , ', get_the_id()); ?>
        <i class="fa fa-user"></i><?php the_author_link(); ?>
    </div>
    <div class="post-row-content">
    	<?php the_content(); ?>
    </div>    
	<?php endwhile; endif; ?>
       <!-- post navigation -->
    <div class="navigation cf float">
    <?php if( get_previous_post() ): ?>
    <div style="float:left;"><?php previous_post_link('%link', '« %title'); ?></div>
    <?php endif;
    if( get_next_post() ): ?>
    <div style="float:right;"><?php next_post_link('%link', '%title »'); ?></div>
    <?php endif; ?>
    </div>
    <!-- /post navigation -->	
</div>
<?php get_template_part('footer'); ?>