<nav class="globalNavi-dynamic">
    <div class="container"><!-- begin container -->
        <div class="row clearfix"><!-- begin row -->
            <div class="col-md-18"><!-- begin col -->
            	<div class="globalNavi-content clearfix">					
            		<ul>
            			<li><a href="<?php bloginfo('url'); ?>/">
            				ホーム
            			</a></li>
            			<li><a href="<?php bloginfo('url'); ?>/service">
            				サービス案内
            			</a></li>
            			<li><a href="<?php bloginfo('url'); ?>/ori_list">
            				折り加工一覧
            			</a></li>
            			<li><a href="<?php bloginfo('url'); ?>/price">
            				参考価格例
            			</a></li>
                        <li><a href="<?php bloginfo('url'); ?>/flow">
                            ご利用の流れ
                        </a></li>						 
            		</ul>
				</div>                
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
	<script language="javascript">
	$(document).ready(function(){
	    //var globalNavi_width = parseInt( $('.globalNavi-dynamic .globalNavi-content').css('width') );   
	    //var globalNavi_items = parseInt( $('.globalNavi-dynamic .globalNavi-content > ul > li').size() );  
	    //$('.globalNavi-dynamic .globalNavi-content > ul > li').css('width', eval(globalNavi_width/globalNavi_items) + 'px' );
	});
	</script>     
</nav>