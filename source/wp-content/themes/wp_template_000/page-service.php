<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>	   
	<div class="primary-row clearfix">	
		<h2 class="h2_title">お急ぎのお客様へ</h2>  
		<div class="service-bg">
			<p>
			<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img1.jpg" alt="page feature" />
			</p>
			<div class="message-left message-141 clearfix">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img2.jpg" alt="page feature" />
				</div>
				<div class="text ln2em">				
					<p>
						弊社では、短納期をご希望のお客様にも対応ができるように設備の準備をしております。<br />
						「２つ折り」「外３つ折り」「巻き三つ折り」などの基本の折り方であれば、最短納期即日での出荷が可能です。<br />
					</p>				
				</div>			
			</div>
			<div class="service-box1">
				<div class="service-box1-title">
					短納期パックがご利用できる折り加工			
				</div>
				<ul class="clearfix">
					<li>
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_box1_img1.jpg" alt="page feature" />
						<p class="text-center pt10">2つ折り</p>
					</li>
					<li>
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_box1_img2.jpg" alt="page feature" />
						<p class="text-center pt10">外３つ折り</p>
					</li>
					<li>
						<img src="<?php bloginfo('template_url'); ?>/img/content/service_box1_img3.jpg" alt="page feature" />
						<p class="text-center pt10">巻き３つ折り</p>
					</li>
				</ul>
			</div>
			<div class="service-box1-info">
				※枚数によっては他の折り加工も対応可能です。お気軽にご相談ください。
			</div>
			<p class="ln2em">
				短納期パックにが適応できる条件がいくつかありますのでご確認ください。<br />
				また、お急ぎの場合は事前にご相談ください。
			</p>	

			<div class="service-frame-text ln2em">
				<p><span class="service-frame-text-step">条件1</span>…「 ２つ折り」「外３つ折り」「巻き三つ折り」などの基本の折り方の場合</p>
				<p><span class="service-frame-text-step">条件2</span>…１万枚以下のご注文の場合</p>
				<p><span class="service-frame-text-step">条件3</span>…当日正午１２時までに弊社まで印刷物を支給できる場合</p>
				<p class="pt10">上記３つの条件をすべて満たした場合は、最短即日出荷が可能です！</p>
			</div>
			<div class="service-text ln2em">
				<p><span class="service-text-title">※ご注意※</span></p>
				<p class="pl10">
					・当日にご相談されてもお受けできない場合がございます。お急ぎの場合は事前にご相談ください。<br />
					・出荷先が遠方の場合は、即日出荷されても運送が間に合わない可能性があります。<br />
					　予め余裕を持ってご注文ください。<br />
					・時期によってはお受けできない場合もございます。ご了承ください。
				</p>
			</div>
		</div>				
	</div>		
	<?php get_template_part('part','contact'); ?>
	
	<div class="primary-row clearfix">	
		<h2 class="h2_title">印刷会社・企画会社様へ</h2>  
		<div class="service-bg">
			<p>
			<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img4.jpg" alt="page feature" />
			</p>
			<div class="service-group"><!-- begin service-group -->			
				<div class="service-row clearfix">				
					<div class="service-col service-col360 clearfix">
						<div class="service-box2">
							<div class="service-box2-text1">
								クライアントの予算内に抑えたい…
							</div>
							<div class="service-box2-text2">
								完全納得！クライアント様、そして代理店様の予算内に収まるご提案を差し上げます！
							</div>
						</div>					
					</div><!-- end service-col -->
					<div class="service-col service-col360 clearfix">
						<div class="service-box2">
							<div class="service-box2-text1">
								印刷がギリギリになりそう…
							</div>
							<div class="service-box2-text2 clearfix">
								納期のご相談承ります。即日出荷可能な「短納期パック」もご用意しております。仕様によっては難しい場合もございますので、お早めにご相談ください。
							</div>
						</div>				
					</div><!-- end service-col -->					
				</div><!-- end service-row -->	

				<div class="service-row clearfix">				
					<div class="service-col service-col360 clearfix">
						<div class="service-box2">
							<div class="service-box2-text1">
								どんな折り方がいいのか相談したい
							</div>
							<div class="service-box2-text2">
								企画やデザイン段階でのご相談もお待ちしております。「掲載ボリュームが多いけどコンパクトに仕上げたい」「なるべく価格を抑えたい」「見せ方を工夫したい」など折り加工に関するお悩みを一緒に解決いたします。
							</div>
						</div>					
					</div><!-- end service-col -->
					<div class="service-col service-col360">
						<div class="image text-center">
							<img src="<?php bloginfo('template_url'); ?>/img/content/service_box2_img.jpg" alt="page feature" />
						</div><!-- end image -->						
					</div><!-- end service-col -->					
				</div><!-- end service-row -->
				
			</div><!-- end service-group -->	
		</div>
		
	</div>
	
	<div class="primary-row clearfix">	
		<h2 class="h2_title">個人様、ショップオーナー様へ</h2>  
		<div class="service-bg clearfix">
			<p>
				弊社では最小100部からの小ロット対応もしております。<br />
				個人様や店舗・ショップのオーナー様で、少部数だけどパンフレットやチラシが必要になった場合などでも、活用いただけます。
			</p>
			<div class="service-group"><!-- begin service-group -->			
				<div class="service-row clearfix">				
					<div class="service-col service-col240 clearfix">
						<div class="servie-box3">
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/service_box3_img1.jpg" alt="page feature" />
							</div>
							<div class="text">
								士業や自営業の事務所ご案内　のパンフレット制作に。
							</div>
						</div>					
					</div><!-- end service-col -->
					<div class="service-col service-col240 clearfix">
						<div class="servie-box3">
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/service_box3_img2.jpg" alt="page feature" />
							</div>
							<div class="text">
								大切な結婚式に手作りの招待状を作られる場合に。
							</div>
						</div>					
					</div><!-- end service-col -->
					<div class="service-col service-col240 clearfix">
						<div class="servie-box3">
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/service_box3_img3.jpg" alt="page feature" />
							</div>
							<div class="text">
								イベントの資料を折り込まれる場合や、会場での配布用に。
							</div>
						</div>					
					</div><!-- end service-col -->					
				</div><!-- end service-row -->	

				<div class="service-row clearfix">				
					<div class="service-col service-col240 clearfix">
						<div class="servie-box3">
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/service_box3_img4.jpg" alt="page feature" />
							</div>
							<div class="text">
								ショップの折り込みチラシ、店頭での配布用に。
							</div>
						</div>					
					</div><!-- end service-col -->
					<div class="service-col service-col240 clearfix">
						<div class="servie-box3">
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/service_box3_img5.jpg" alt="page feature" />
							</div>
							<div class="text">
								企業のパンフレットや製品のカタログの制作に。
							</div>
						</div>					
					</div><!-- end service-col -->
					<div class="service-col service-col240 clearfix">
						<div class="servie-box3">
							<div class="image">
								<img src="<?php bloginfo('template_url'); ?>/img/content/service_box3_img6.jpg" alt="page feature" />
							</div>
							<div class="text">
								ポスティング、折り込、街頭配布など様々なDM配布に。
							</div>
						</div>					
					</div><!-- end service-col -->					
				</div><!-- end service-row -->	
				
			</div><!-- end service-group -->	
			<p class="pt10">
				など、様々な印刷物の折り加工・断裁を承ります。<br />
				印刷・綴じ加工も合わせてご相談ください。お気軽にお問い合わせください。
			</p>
		</div>		
	</div>	
	
	<div class="pt40 clearfix">	
		<h2 class="h2_title">紙折り加工なら「折り加工専門工房」へお任せください！</h2>  
		<div class="service-navi-bg clearfix">
			<p class="pt20 pb20">
				弊社ではパンフレット・チラシ・DMの「折り加工」を専門に行っています。		
			</p>
			<div class="service-ori">
				<div class="image">
					<img src="<?php bloginfo('template_url'); ?>/img/content/service_content_img5.jpg" alt="page feature" />
				</div>
				<div class="text">
					<p class="service-ori-title">折り加工</p>
					<div class="service-ori-navi clearfix">
						<ul class="clearfix">
							<li><a href="<?php bloginfo('url'); ?>/ori">2つ折り</a></li>
							<li><a href="<?php bloginfo('url'); ?>/ori">巻き３つ折り</a></li>
							<li><a href="<?php bloginfo('url'); ?>/ori">ミニ折り</a></li>
							<li><a href="<?php bloginfo('url'); ?>/ori">十文字（８P）折り</a></li>	
							<li><a href="<?php bloginfo('url'); ?>/ori">ピラミッド折り</a></li>															
						</ul>							
						<ul class="clearfix">
							<li><a href="<?php bloginfo('url'); ?>/ori">外３つ折り</a></li>
							<li><a href="<?php bloginfo('url'); ?>/ori">観音折り</a></li>
							<li><a href="<?php bloginfo('url'); ?>/ori">蛇腹（じゃばら）折り</a></li>
							<li><a href="<?php bloginfo('url'); ?>/ori">１６P折り</a></li>			
						</ul>
					</div>					
					<p class="mt20">●その他、断裁加工も自社工場で行っています。</p>
				</div>
			</div>
		</div>		
	</div>	
	
	
<?php get_template_part('part','contact'); ?>
<?php get_footer(); ?>