<?php get_header(); ?>
<?php get_template_part('part','breadcrumb'); ?>
	<div class="primary-row clearfix">
		<h2 class="h2_title">よくある質問と回答</h2>
		<div class="faq-bg">
			<div class="primary-row clearfix">
			<h3 class="h3_title">よくある質問と回答</h3>
			<div class="ln2em">
				もちろん大歓迎です。弊社は折り加工を専門に行っている会社ですので、印刷会社から折り加工だけ依頼される案件が殆どです。印刷会社とのやりとりも経験が豊富であります。納期・価格などのご相談もお気軽のお問い合わせください。
			</div>
			</div><!-- end primary-row1 -->
			<div class="primary-row clearfix">
				<h3 class="h3_title">急ぎの案件ですが対応できますか？</h3>
				<div class="ln2em">
					２つ折り、３つ折りなどの基本的な折り方で１万枚まででしたら、午前中までに印刷物の支給いただければ当日発送も可能です。（部数が少なければ他の折り方でも即日発送可能な場合はございます）<br />
					その他、納期と部数をご相談いただければ、なるべくご希望に添えるように調整いたします。<br />
					いずれの場合もできるだけ早めにご連絡いただければ幸いです。
				</div>
			</div><!-- end primary-row2 -->
			<div class="primary-row clearfix">
				<h3 class="h3_title">個人で小ロットですが依頼できますか？</h3>
				<div class="ln2em">
					個人のお客様も歓迎いたします。最小ロットは100枚ですが、それより少ない部数をご希望の場合もまずは１度後ご相談ください。
				</div>
			</div><!-- end primary-row3 -->
			<div class="primary-row clearfix">
				<h3 class="h3_title">厚紙でも折り加工できますか？</h3>
				<div class="ln2em">
					紙の厚さは135kgまでがキレイに折れる推奨の厚みです。135kgとは、だいたい雑誌についている薄手のハガキ程度の厚さです。<br />
					それ以上の厚みでも対応できる場合もありますが、折り目にしわが出来たり、印刷に1mm前後のズレが生じますのであまりおすすめはいたしません。
				</div>
			</div><!-- end primary-row4 -->
			<div class="primary-row clearfix">
				<h3 class="h3_title">紙の素材はどんなものが対応可能ですか？</h3>
				<div class="ln2em">
					印刷できる紙であれば、大体どんな紙素材でも対応可能です。<br />
					ただ、あまりに分厚い紙や、ユポ紙などの極端に腰のない紙は、機械に通すことができないので折ることが難しくなります。
				</div>
			</div><!-- end primary-row5 -->
			<div class="primary-row clearfix">
				<h3 class="h3_title">印刷や、綴じ加工、抜き加工、裁断なども一緒に頼めますか？</h3>
				<div class="ln2em">
					可能です。弊社では折り加工の他に裁断加工も専門に行っております。<br />
					また、印刷会社とのお取引も多くありますので、お客様のご希望にあう印刷会社の紹介や、提携先の綴じ加工、抜き加工と協力して制作することができます。
				</div>
			</div><!-- end primary-row6 -->
		</div>		
	</div><!-- end primary-row -->
<?php get_footer(); ?>