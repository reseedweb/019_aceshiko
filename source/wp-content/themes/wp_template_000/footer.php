
                                </main><!-- end primary -->
                                <aside class="sidebar"><! begin sidebar -->
                                    <?php if(is_page('blog') || is_category() || is_single()) : ?>
                                        <?php
                                        $queried_object = get_queried_object();                                
                                        $sidebar_part = 'blog';
                                        if(is_tax() || is_archive()){                                    
                                            $sidebar_part = '';
                                        }                               

                                        if($queried_object->post_type != 'post' && $queried_object->post_type != 'page'){
                                            $sidebar_part = '';
                                        }   
                                                
                                        if($queried_object->taxonomy == 'category'){                                    
                                            $sidebar_part = 'blog';
                                        }                 
                                        ?>
                                        <?php get_template_part('sidebar',$sidebar_part); ?>  
                                    <?php else: ?>
                                        <?php get_template_part('sidebar'); ?>  
                                    <?php endif; ?>                                    
                                </aside>
                            </div><!-- end two-cols -->
                        </div><!-- end col -->
                    </div><!-- end row -->
                </div><!-- end container -->            
            </section><!-- end content -->

            <footer><!-- begin footer -->
                <section class="footer-nav">
                    <div class="container"><div class="row clearfix"><div class="col-md-18">
                        <ul class="clearfix">
                            <li><a href="<?php bloginfo('url'); ?>/">ホーム</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/service">サービス案内</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/ori_list">折り加工一覧</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/price">参考価格例</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/flow">ご利用の流れ</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/contact">お問い合わせ</a></li>
                        </ul>
                    </div></div></div>
                </section>
                <section class="footer-company">
                <div class="container"><!-- begin container -->
                    <div class="row clearfix"><!-- begin row -->
                        <div class="col-md-18"><!-- begin col -->
                            <p class="text-center">
                                <a href="<?php bloginfo('url'); ?>/">
                                    <img alt="logo" 
                                    src="<?php bloginfo('template_url');?>/img/common/footer_logo.jpg" />                         
                                </a>
                            </p>                 
                            <p class="text-center">                                
                                <img alt="tel" 
                                src="<?php bloginfo('template_url');?>/img/common/footer_tel.jpg" />                                                         
                            </p>                                                        
                        </div>
                    </div>
                </div>                    
                </section>
                <section class="footer-copyright">
                    <div class="container"><!-- begin container -->
                        <div class="row clearfix"><!-- begin row -->
                            <div class="col-md-18"><!-- begin col -->                                
                                <div class="text-center">
                                    Copyright©2014 有限会社エース紙工 All Rights Reserves.
                                </div>                                
                            </div><!-- end col -->
                        </div><!-- end row -->
                    </div><!-- end container -->            
                </section>
            </footer><!-- end footer -->            

        </div><!-- end wrapper -->
        <?php wp_footer(); ?>        
        <script language="javascript">
        $(document).ready(function(){
            var footerNav_width = parseInt( $('.footer-nav ul').css('width') );   
            var footerNav_items = parseInt( $('.footer-nav ul li').size() );  
            $('.footer-nav ul li').css('width', eval(footerNav_width/footerNav_items) + 'px' );
        });
        </script>                 
    </body>
</html>