<div class="sidebar-row clearfix"><!-- begin sidebar-row -->
    <div class="side-con"><!-- begin side-con -->
        <img alt="contact bg" 
        src="<?php bloginfo('template_url');?>/img/common/side_con_bg.jpg" />
        <div class="side-con-btn">
            <a href="<?php bloginfo('url'); ?>/contact">
                <img alt="contact" 
                src="<?php bloginfo('template_url');?>/img/common/side_con_btn.jpg" />
            </a>
        </div>
    </div><!-- end side-con -->
</div><!-- end sidebar-row -->

<div class="sidebar-row"><!-- begin sidebar-row -->
    <a href="<?php bloginfo('url'); ?>/link">
        <img alt="side bnr" 
        src="<?php bloginfo('template_url');?>/img/common/side_bnr1.jpg" />                         
    </a>
</div><!-- end sidebar-row -->

<div class="sidebar-row clearfix">
	<div class="sideBlog">
		<h3 class="sideBlog-title">最新の投稿</h3>
		<ul>
			<?php query_posts("posts_per_page=5"); ?><?php if(have_posts()):while(have_posts()):the_post(); ?>
			<li>
			<p><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></p>
			</li>
			<?php endwhile;endif; ?>
		</ul> 
	</div>
	<div class="sideBlog">
	    <h3 class="sideBlog-title">最新の投稿</h3>
	      <ul class="category">
			<?php wp_list_categories('sort_column=name&optioncount=0&hierarchical=1'); ?>
	      </ul>                             	
	</div>
	<div class="sideBlog">
      <h3 class="sideBlog-title">最新の投稿</h3>
      <ul><?php wp_get_archives('type=monthly&limit=12'); ?></ul>                               	
	</div>
</div>
